using Michsky.MUIP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContextMenu : MonoBehaviour
{
    [SerializeField] private ContextMenuManager myContextMenu; // Variable
    public void OpenMenu()
    {
        myContextMenu.Open(); // Open menu
    }
}
