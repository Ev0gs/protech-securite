using Michsky.MUIP;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorsManager : MonoBehaviour
{
    [SerializeField] private GameObject goToManage = default;
    [SerializeField] private HorizontalSelector selector = default;

    public void HorizontalSelectorChanged() {
        if (selector.index > 2) goToManage.SetActive(false);
        else goToManage.SetActive(true);
    }
}

