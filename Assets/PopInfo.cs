using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopInfo : MonoBehaviour
{
    [SerializeField] private Camera mainCamera = default;
    [SerializeField] private GameObject objectOne;    // The first object you instantiate
    [SerializeField] private GameObject objectTwo;    // The second object you instantiate

    [SerializeField] private LineRenderer lineRend;   // The linerenderer component, remember to assign this in the inspector!
    [SerializeField] private float animationDuration = 5f;

    private bool isAnimating = false;

    private void Start()
    {
        lineRend.positionCount = 2;
        //StartCoroutine(AnimateLine());
    }

    void Update()
    {
        this.transform.LookAt(transform.position + mainCamera.transform.rotation * Vector3.forward, mainCamera.transform.rotation * Vector3.up);
        DrawLineBetweenObjects();
    }

    void DrawLineBetweenObjects()
    {
        lineRend.SetPosition(0, objectOne.transform.position);
        /*if (!isAnimating)*/  lineRend.SetPosition(1, objectTwo.transform.position);
    }

    private IEnumerator AnimateLine()
    {
        isAnimating = true;
        float segmentDuration = animationDuration ;
        float startTime = Time.time;

        Vector3 startPosition = objectOne.transform.position;
        Vector3 endPosition = objectTwo.transform.position;

        Vector3 pos = startPosition;
        while (pos != endPosition)
        {
            float t = (Time.time - startTime) / segmentDuration;
            pos = Vector3.Lerp(startPosition, endPosition, t);          
            lineRend.SetPosition(1, pos);
            endPosition = objectTwo.transform.position;
            yield return null;
        }
        isAnimating = false;
        DrawLineBetweenObjects();
    }
}
