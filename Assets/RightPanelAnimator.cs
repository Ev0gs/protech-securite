using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightPanelAnimator : MonoBehaviour
{
    [SerializeField] private GameObject blur = default;
    public void OpenPanel()
    {
        blur.SetActive(true);
        GetComponent<Animation>().Play();
    }

    public void ClosePanel()
    {
        blur.SetActive(false);
        GetComponent<Animation>().Rewind();
    }
}
