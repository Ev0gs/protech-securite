using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguageSelectorUI : MonoBehaviour
{
    [SerializeField] private Button frenchButton = default;
    [SerializeField] private Button englishButton = default;
    private Color32 deselectColor = new Color32(255, 255, 255, 100);
    private Color32 selectColor = new Color32(255, 255, 255, 255);

    private void Start()
    {
        SetFrench();
    }

    public void SetFrench()
    {
        ColorBlock colors = frenchButton.colors;
        colors.normalColor = selectColor;
        frenchButton.colors = colors;
        
        ColorBlock colors2 = englishButton.colors;
        colors2.normalColor = deselectColor;
        englishButton.colors = colors2;
    }

    public void SetEnglish()
    {
        ColorBlock colors = frenchButton.colors;
        colors.normalColor = deselectColor;
        frenchButton.colors = colors;

        ColorBlock colors2 = englishButton.colors;
        colors2.normalColor = selectColor;
        englishButton.colors = colors2;
    }

}
