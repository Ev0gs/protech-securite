using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RankSelectorUI : MonoBehaviour
{
    [SerializeField] private Button rank1 = default;
    [SerializeField] private Button rank2 = default;
    [SerializeField] private Button rank3 = default;
    [SerializeField] private Button all = default;

    private Color32 deselectColor = new Color32(255, 255, 255, 100);
    private Color32 selectColor = new Color32(255, 255, 255, 255);

    public static RankSelectorUI Instance;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this);
    }

    private void Start()
    {
        SetAll();
    }

    public void SetAll()
    {
        ColorBlock colors = all.colors;
        ColorBlock colors2 = all.colors;

        colors.normalColor = selectColor;
        colors2.normalColor = deselectColor;

        all.colors = colors;
        rank1.colors = colors2;
        rank2.colors = colors2;
        rank3.colors = colors2;
    }

    public void SetRank1()
    {
        ColorBlock colors = all.colors;
        ColorBlock colors2 = all.colors;

        colors.normalColor = selectColor;
        colors2.normalColor = deselectColor;

        all.colors = colors2;
        rank1.colors = colors;
        rank2.colors = colors2;
        rank3.colors = colors2;
    }

    public void SetRank2()
    {
        ColorBlock colors = all.colors;
        ColorBlock colors2 = all.colors;

        colors.normalColor = selectColor;
        colors2.normalColor = deselectColor;

        all.colors = colors2;
        rank1.colors = colors2;
        rank2.colors = colors;
        rank3.colors = colors2;
    }

    public void SetRank3()
    {
        ColorBlock colors = all.colors;
        ColorBlock colors2 = all.colors;

        colors.normalColor = selectColor;
        colors2.normalColor = deselectColor;

        all.colors = colors2;
        rank1.colors = colors2;
        rank2.colors = colors2;
        rank3.colors = colors;
    }
}
