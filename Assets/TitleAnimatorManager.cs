using Michsky.MUIP;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TitleAnimatorManager : MonoBehaviour
{
    [SerializeField] private Animator titleAnimator;
    [SerializeField] private TextMeshProUGUI elementTitle;
    [SerializeField] private TextMeshProUGUI elementTitleHelper;

    private List<GameObject> elements = new List<GameObject>();
    private int prevIndex;

    void Awake()
    {
        foreach (Transform child in transform)
        {
            elements.Add(child.gameObject);
        }
    }
    public void SetWindowManagerButton(int index)
    {

        if (titleAnimator == null)
            return;

        elementTitleHelper.text = elements[prevIndex].name;
        elementTitle.text = elements[index].name;

        titleAnimator.Play("Idle");
        titleAnimator.Play("Transition");

        prevIndex = index;
    }
}
