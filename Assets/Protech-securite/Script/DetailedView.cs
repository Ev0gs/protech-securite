using Michsky.MUIP;
using System.Collections;
using System.Collections.Generic;
using UI.ThreeDimensional;
using UnityEngine;

public class DetailedView : MonoBehaviour
{
    public UIObject3D Slot;
    public UIObject3DImage SlotImage;
    public CameraInteractions CameraInteractions;
    public HorizontalSelector horizontalSelector;
    public List<ProductDetails> productsScob;
    public ContentManager contentManager;
    public CameraInteractions cameraInteractions;
/*    public void SetObject(Transform obj)
    {
        obj.SetParent(Slot.transform);
    }
    
    public void SetObjectFromItemsView(Transform obj)
    {
        Slot.ObjectPrefab = obj;
    }*/

    public void SetObjectFromItemView()
    {
        cameraInteractions.PauseInteractions(true);
        contentManager.SetContent(productsScob[horizontalSelector.index]);
    }
}
