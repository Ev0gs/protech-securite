﻿using Ifooboo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;

namespace Assets.Protech_securite.Script
{
    public class CustomFader : MonoBehaviour
    {
            private List<Renderer> renderers = new List<Renderer>();

            private List<Material> materials = new List<Material>();

            private int materialCount;

            private int hashFade = Shader.PropertyToID("_IntensityTransparentMap");

            private void Awake()
            {
                renderers = this.GetComponents<Renderer>().ToList();
               
                int listCount = renderers.Count;

                for (int i = 0; i < listCount; i++)
                {
                    int arrayLength = renderers[i].materials.Length;

                    for (int j = 0; j < arrayLength; j++)
                    {
                        materials.Add(renderers[i].materials[j]);
                    }
                }

                materialCount = materials.Count;
            }

            public void FadeMaterials(float value, bool showDefaultVitre = false)
            {
                for (int i = 0; i < materialCount; i++)
                {
                    if(materials[i].name != "Vitre (Instance)") materials[i].SetFloat("_IntensityTransparentMap", value);
                    else if(materials[i].name == "Vitre (Instance)")
                    {
                    if (showDefaultVitre) materials[i].SetFloat("_IntensityTransparentMap", 0.85f);
                    else if (value == 0) materials[i].SetFloat("_IntensityTransparentMap", 0.5f);
                    else if (value == 0.9f) materials[i].SetFloat("_IntensityTransparentMap", 0.9f);
                    else if (value == 1f) materials[i].SetFloat("_IntensityTransparentMap", 1f);
                    }
                }
            }
    }
}