using Assets.Protech_securite.Script;
using Michsky.MUIP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;

public class CameraInteractions : MonoBehaviour
{
    [SerializeField]private WindowManager windowsManager = default;
    [SerializeField] private DetailedView detailedView = default;
    [HideInInspector] public UnityEvent ZoomEvent;
    [HideInInspector] public UnityEvent<Touch?> MovementEvent;
    private int CurrentFocusItem = -99;
    [Header("Zoom")]
    Camera mainCamera;
    [SerializeField] private Camera uiCamera = default;
    float touchesPrevPosDifference, touchesCurPosDifference, zoomModifier, previousMouseScroll;
    Vector2 firstTouchPrevPos, secondTouchPrevPos;
    [SerializeField][Range(0.0f, 5.0f)] float ZoomSpeed = 0.01f;

    [Header("OrbitalView")]
    [SerializeField] private UnityEngine.UI.Slider animSlider = default;
    public Transform target;
    [SerializeField] private Transform mainTarget;
    private Vector3 previousPosition;
    [Range(0.0f, 5.0f)] public float Speed = 0.01f;

    [Header("Items View")]
    [SerializeField] private List<Transform> itemsTargets = default;

    [Header("Fader Groups")]
    [SerializeField] private List<CustomFader> fadeRang1 = default;
    [SerializeField] private List<CustomFader> fadeRang2 = default;
    [SerializeField] private List<CustomFader> fadeRang3 = default;
    [SerializeField] private List<CustomFader> wallsToHide = default;

    [Header("PopInfo Groups")]
    [SerializeField] private List<PopInfo> popInfoRang1 = default;
    [SerializeField] private List<PopInfo> popInfoRang2 = default;
    [SerializeField] private List<PopInfo> popInfoRang3 = default;

    [Header("All faders")]
    [SerializeField] private List<CustomFader> faders = default;
    //States
    private bool isPaused = false;
    private bool isSliderInTouch = false;
    private bool isInteriorShowed = false;
    private bool isTouching = false;

    public Quaternion currentRot; // store the quaternion after the slerp operation
    public Quaternion targetRot;
    public Vector3 dir;
    private float distanceBetweenCameraAndTarget;

    //shaker variables
    public float shakeAmount = 0.7f;
    public float shakeDuration = 0.5f;
    public float decreaseFactor = 1.0f;
    private Vector3 originalPos;
    private Quaternion originalRot;
    private bool isShaking = false;


    private void Update()
    {
        if (isPaused || isSliderInTouch) return;
        if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            //EditorCameraInput();
            Zoom();
        }
    }

    private void LateUpdate()
    {
        if (isPaused || isSliderInTouch) return;
        if (Application.isEditor || Application.platform == RuntimePlatform.WindowsPlayer) RotateCamera();
    }
    private void Start()
    {
        mainCamera = GetComponent<Camera>();
        distanceBetweenCameraAndTarget = Vector3.Distance(mainCamera.transform.position, target.position);
        dir = new Vector3(0, 0, distanceBetweenCameraAndTarget);
        // Zoom Event Init
        if (ZoomEvent == null)
            ZoomEvent = new UnityEvent();

        ZoomEvent.AddListener(ZoomTouch);

        // Movement Event Init
        if (MovementEvent == null)
            MovementEvent = new UnityEvent<Touch?>();
        
        MovementEvent.AddListener(Movement);
       

        // Component Init
        mainCamera.transform.position = new Vector3(target.position.x, target.position.y, target.position.z -10);
        uiCamera.transform.position = mainCamera.transform.position;
    }

    public void OnSliderTouch(bool isDragging)
    {
        isSliderInTouch = isDragging;
    }

    public void PauseInteractions(bool isPaused)
    {
        this.isPaused = isPaused;
    }

    //orbit camera around target with mouse input

    private void RotateCamera()
    {
    
        if (isPaused || isSliderInTouch) return;
        if (isTouching) return;
        if (Input.GetMouseButtonDown(0))
        {
            previousPosition = mainCamera.ScreenToViewportPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            // Orbit view script
            mainCamera.transform.position = target.position;
            Vector3 direction = previousPosition - mainCamera.ScreenToViewportPoint(Input.mousePosition);
            mainCamera.transform.Rotate(new Vector3(1, 0, 0), direction.y * 180 * Speed);
            mainCamera.transform.Rotate(new Vector3(0, 1, 0), -direction.x * 180 * Speed, Space.World);
            mainCamera.transform.Translate(new Vector3(0, 0, -10));
            uiCamera.transform.SetPositionAndRotation(mainCamera.transform.position, mainCamera.transform.rotation);
            previousPosition = mainCamera.ScreenToViewportPoint(Input.mousePosition);
        }
    }


    #region Movement

    private void Movement(Touch? externalInput = null)
    {
        if (isPaused || isSliderInTouch) return;

        


        // Orbit view script
        Touch touch = Input.GetTouch(0);

        switch (touch.phase)
        {
            case TouchPhase.Began:
                isTouching = true;
                Debug.Log("Is touching true");
                previousPosition = mainCamera.ScreenToViewportPoint(touch.position);
                break;
            case TouchPhase.Moved:
                mainCamera.transform.position = target.position;
                Vector3 direction = previousPosition - mainCamera.ScreenToViewportPoint(touch.position);
                mainCamera.transform.Rotate(new Vector3(1, 0, 0), direction.y * 180 * Speed);
                mainCamera.transform.Rotate(new Vector3(0, 1, 0), -direction.x * 180 * Speed, Space.World);
                mainCamera.transform.Translate(new Vector3(0, 0, -10));
                uiCamera.transform.SetPositionAndRotation(mainCamera.transform.position, mainCamera.transform.rotation);
                previousPosition = mainCamera.ScreenToViewportPoint(touch.position);
                break; 
            case TouchPhase.Ended:
                
                isTouching = false;
                Debug.Log("Is touching false");
                break;
        }
    }
#endregion


#region Zoom
    private void Zoom()
    {
        if (isPaused || isSliderInTouch) return;
        float minZoom = 1f;
        float maxZoom = 10f;
        float sensitivity = 10f;
        float ortoSize = Camera.main.orthographicSize;
        //zoomModifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * ZoomSpeed;
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        if (mouseScroll != previousMouseScroll)
        {
            ortoSize += mouseScroll;
            ortoSize = Mathf.Clamp(ortoSize, minZoom, maxZoom);
            Camera.main.orthographicSize = ortoSize;
            uiCamera.orthographicSize = mainCamera.orthographicSize;
            previousMouseScroll = mouseScroll;
        }
    }

    private void ZoomTouch()
    {
        if (isPaused || isSliderInTouch) return;
        Touch firstTouch = Input.GetTouch(0);
        Touch secondTouch = Input.GetTouch(1);

        firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
        secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

        touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
        touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

        zoomModifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * ZoomSpeed;

        if (touchesPrevPosDifference > touchesCurPosDifference)
        {
            mainCamera.orthographicSize += zoomModifier;
        }

        if (touchesPrevPosDifference < touchesCurPosDifference)
        {
            mainCamera.orthographicSize -= zoomModifier;
        }
        mainCamera.orthographicSize = Mathf.Clamp(mainCamera.orthographicSize, 0.5f, 10f);
        uiCamera.orthographicSize = mainCamera.orthographicSize;

        // Fix camera moving after finishing zoom
        if (firstTouch.phase == TouchPhase.Ended)
        {
            previousPosition = mainCamera.ScreenToViewportPoint(secondTouch.position);
        }
        if (secondTouch.phase == TouchPhase.Ended)
        {
            previousPosition = mainCamera.ScreenToViewportPoint(firstTouch.position);
        }
    }
    #endregion

    #region ItemView

    public void InitFocusItemScreen()
    {
        ChangeFocusItem(detailedView.horizontalSelector.index);
    }

    public void ChangeFocusItem(int index)
    {
        CurrentFocusItem = index;
        //-99 is main target for orbital view 
        if (index == -99)
        {
            faders.ForEach((fader) => fader.FadeMaterials(0f, true));
            this.target = this.mainTarget;
            mainCamera.transform.position = target.position;
            mainCamera.transform.Translate(new Vector3(0, 0, -10));
        }
        else if(index == 4 && windowsManager.currentWindowIndex != 1)
        {
            isInteriorShowed = !isInteriorShowed;
            if(isInteriorShowed)wallsToHide.ForEach((fader) => fader.FadeMaterials(1f));
            else wallsToHide.ForEach((fader) => fader.FadeMaterials(0f, true));
        }
        else
        {
            faders.ForEach((fader) => fader.FadeMaterials(0.9f));
            this.target = this.itemsTargets[index];
            this.target.GetComponent<CustomFader>().FadeMaterials(0f);
            mainCamera.transform.position = target.position;
            mainCamera.transform.Translate(new Vector3(0, 0, -10));
        }
    }

    public void FocusGroup(int group)
    {
        switch (group)
        {
            case 1:
                faders.ForEach((fader) => fader.FadeMaterials(0.9f));
                fadeRang1.ForEach((fader) => fader.FadeMaterials(0f));
                popInfoRang1.ForEach((pop) => pop.gameObject.SetActive(true));
                popInfoRang2.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang3.ForEach((pop) => pop.gameObject.SetActive(false));
                break;
            case 2:
                faders.ForEach((fader) => fader.FadeMaterials(0.9f));
                fadeRang2.ForEach((fader) => fader.FadeMaterials(0f));
                popInfoRang1.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang2.ForEach((pop) => pop.gameObject.SetActive(true));
                popInfoRang3.ForEach((pop) => pop.gameObject.SetActive(false));
                break;
            case 3:
                faders.ForEach((fader) => fader.FadeMaterials(0.9f));
                fadeRang3.ForEach((fader) => fader.FadeMaterials(0f));
                popInfoRang1.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang2.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang3.ForEach((pop) => pop.gameObject.SetActive(true));
                break;
            case -99:
                ChangeFocusItem(-99);
                popInfoRang1.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang2.ForEach((pop) => pop.gameObject.SetActive(false));
                popInfoRang3.ForEach((pop) => pop.gameObject.SetActive(false));
                break;
        }
    }

#endregion
}