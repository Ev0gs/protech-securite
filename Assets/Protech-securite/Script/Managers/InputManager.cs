using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public CameraInteractions cameraScript;

    private static InputManager instance = null;
    public static InputManager Instance => instance;

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (Input.touchCount == 1)
        {
            // Activate camera movement
            cameraScript.MovementEvent?.Invoke(default);
        }
        if(Input.touchCount == 2)
        {
            // Activate camera zoom
            cameraScript.ZoomEvent?.Invoke();
        }
    }
}
