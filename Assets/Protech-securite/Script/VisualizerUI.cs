using Lean.Localization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualizerUI : MonoBehaviour
{
    public GameObject ObjectToVisualize;
    private Animator _objectAnimator;
    [SerializeField] private Slider animSlider = default;
    private bool _isViewExploded = false;

    private void Start()
    {
        _objectAnimator = ObjectToVisualize.GetComponent<Animator>();
        animSlider.onValueChanged.AddListener(SetAnimationTime);
    }

    private void OnDestroy()
    {
        animSlider.onValueChanged.RemoveAllListeners();
    }

    public void ExplodedView()
    {
        if (_isViewExploded)
        {
            _objectAnimator.SetTrigger("Assembled");
        }
        else
        {
            _objectAnimator.SetTrigger("Exploded");
        }
        _isViewExploded = !_isViewExploded;
    }


    public void SetAnimationTime(float value)
    {
        _objectAnimator.speed = 0f;
        _objectAnimator.Play("Exploded State", 0, animSlider.value);
    }
}
