﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "ProTech", menuName = "ScriptableObjects/ProductDetails", order = 1)]
public class ProductDetails : ScriptableObject
{
    public string titleFR;
    public string titleEN;
    public List<string> Paragraph1FR;
    public List<string> Paragraph1EN;
    public List<Sprite> imagesGrid;
    public List<string> Paragraph2FR;
    public List<string> Paragraph2EN;
    public string websiteLinkFr;
    public string websiteLinkEN;
}