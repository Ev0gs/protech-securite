using Lean.Localization;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ContentManager : MonoBehaviour
{
    [SerializeField] private GameObject textPrefab = default;
    [SerializeField] private GameObject imagePrefab = default;
    [SerializeField] private TextMeshProUGUI titleText = default;
    [SerializeField] private Transform upperTextContainer = default;
    [SerializeField] private Transform bottomTextContainer = default;
    [SerializeField] private Transform ImageGridContainer = default;
    [SerializeField] private Button openUrlBTN = default;
    [SerializeField] private LeanLocalization localization = default;
    [SerializeField] private Scrollbar scrollbarVertical = default; 

    public void SetContent(ProductDetails productDetails)
    {
        foreach (Transform child in upperTextContainer.transform) Destroy(child.gameObject);
        foreach (Transform child in bottomTextContainer.transform) Destroy(child.gameObject);
        foreach (Transform child in ImageGridContainer.transform) Destroy(child.gameObject);
        
        openUrlBTN.onClick.RemoveAllListeners();

        if(localization.CurrentLanguage == "French")
        {
            titleText.text = productDetails.titleFR;

            productDetails.Paragraph1FR.ForEach(text =>
            {
                string formatedText = text.Replace("�", "\n - ");
                GameObject newText = Instantiate(textPrefab, upperTextContainer);
                newText.GetComponent<TextMeshProUGUI>().text = formatedText;
            });


            productDetails.Paragraph2FR.ForEach(text =>
            {
                string formatedText = text.Replace("�", "\n - ");
                GameObject newText = Instantiate(textPrefab, bottomTextContainer);
                newText.GetComponent<TextMeshProUGUI>().text = formatedText;
            });

            openUrlBTN.onClick.AddListener(() => {
                Application.OpenURL(productDetails.websiteLinkFr);
            });
        }
        else
        {
            titleText.text = productDetails.titleEN;

            productDetails.Paragraph1EN.ForEach(text =>
            {
                string formatedText = text.Replace("�", "\n - ");
                GameObject newText = Instantiate(textPrefab, upperTextContainer);
                newText.GetComponent<TextMeshProUGUI>().text = formatedText;
            });


            productDetails.Paragraph2EN.ForEach(text =>
            {
                string formatedText = text.Replace("�", "\n - ");
                GameObject newText = Instantiate(textPrefab, bottomTextContainer);
                newText.GetComponent<TextMeshProUGUI>().text = formatedText;
            });
            openUrlBTN.onClick.AddListener(() => {
                Application.OpenURL(productDetails.websiteLinkEN);
            });
        }

        productDetails.imagesGrid.ForEach(sprite =>
        {
            GameObject newImage = Instantiate(imagePrefab, ImageGridContainer);
            newImage.GetComponent<Image>().preserveAspect = true;
            newImage.GetComponent<Image>().sprite = sprite;
        });

        scrollbarVertical.value = 1;
    }
}
